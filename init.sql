CREATE TABLE IF NOT EXISTS contracts (
    id VARCHAR(255) PRIMARY KEY,
    base_salary DECIMAL(10, 2) NOT NULL,
    contractType JSON NOT NULL,
    employee JSON NOT NULL,
    end_date DATE NOT NULL,
    position JSON NOT NULL,
    start_date DATE NOT NULL,
    status BOOLEAN NOT NULL
);
