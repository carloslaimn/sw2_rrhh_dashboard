from flask import Blueprint, jsonify, render_template

bp = Blueprint('hello', __name__, url_prefix='/')

@bp.route('/', methods=['GET'])
def hello():
    return render_template('index.html')

