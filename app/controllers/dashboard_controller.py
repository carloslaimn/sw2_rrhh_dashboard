from flask import Blueprint, render_template
from app.controllers.contracts_controller import getAllContractsFromDB

dashboard_bp = Blueprint('dashboard', __name__, url_prefix='/dashboard')

@dashboard_bp.route('/gender', methods=['GET'])
def gender_dashboard():
    response = getAllContractsFromDB()
    contracts = response.get_json()
    employees_by_gender = {'Masculino': 0, 'Femenino': 0}
    for contract in contracts:
        genre = contract['employee']['genre']
        if genre == 'M':
            employees_by_gender['Masculino'] += 1
        elif genre == 'F':
            employees_by_gender['Femenino'] += 1
    
    return render_template('dashboard.html', employees_by_gender=employees_by_gender)

@dashboard_bp.route('/salary', methods=['GET'])
def salary_dashboard():
    response = getAllContractsFromDB()
    contracts = response.get_json()

    salary_by_contract_type = {}
    count_by_contract_type = {}

    for contract in contracts:
        contract_type = contract['contractType']['type']
        base_salary = float(contract['base_salary'])  

        if contract_type in salary_by_contract_type:
            salary_by_contract_type[contract_type] += base_salary
            count_by_contract_type[contract_type] += 1
        else:
            salary_by_contract_type[contract_type] = base_salary
            count_by_contract_type[contract_type] = 1

    average_salary_by_contract_type = {
        k: salary_by_contract_type[k] / count_by_contract_type[k]
        for k in salary_by_contract_type
    }
    return render_template('salary_dashboard.html', average_salary_by_contract_type=average_salary_by_contract_type)

@dashboard_bp.route('/professions', methods=['GET'])
def professions_dashboard():
    response = getAllContractsFromDB()
    contracts = response.get_json()

    # Calcular la cantidad de empleados por profesión
    employees_by_profession = {}

    for contract in contracts:
        profession = contract['employee']['profession']['profession']

        if profession in employees_by_profession:
            employees_by_profession[profession] += 1
        else:
            employees_by_profession[profession] = 1

    # Renderizar el template del dashboard con los datos obtenidos
    return render_template('professions_dashboard.html', employees_by_profession=employees_by_profession)

@dashboard_bp.route('/departments', methods=['GET'])
def department_dashboard():
    response = getAllContractsFromDB()
    contracts = response.get_json()

    # Calcular la cantidad de empleados por departamento
    employees_by_department = {}

    for contract in contracts:
        department = contract['position']['departmentBranch']['department']['department']

        if department in employees_by_department:
            employees_by_department[department] += 1
        else:
            employees_by_department[department] = 1

    # Renderizar el template del dashboard con los datos obtenidos
    return render_template('departments_dashboard.html', employees_by_department=employees_by_department)
