from flask import Blueprint

hello_bp = Blueprint('hello', __name__, url_prefix='/')
contracts_bp = Blueprint('contracts', __name__, url_prefix='/api')

from . import hello_controller, contracts_controller

hello_bp.register_blueprint(hello_controller.bp)
contracts_bp.register_blueprint(contracts_controller.bp)
