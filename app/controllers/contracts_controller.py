from flask import Blueprint, jsonify, current_app
import requests
import json
from app.db import get_db
from faker import Faker
import uuid
from datetime import datetime, timedelta
bp = Blueprint('contracts', __name__, url_prefix='/api')

@bp.route('/contracts', methods=['GET'])
def pullAllContracts():
    api_url = 'http://localhost:8080/contracts'  

    try:
        response = requests.get(api_url)

        if response.status_code == 200:
            contractsList = response.json()  
            insert_contracts_into_db(contractsList)  
        else:
            return jsonify({'error': 'Error al obtener contratos desde API REST'}), response.status_code

    except requests.exceptions.RequestException as e:
        return jsonify({'error': f'Error de conexión: {str(e)}'}), 500

    return jsonify(contractsList)

def insert_contracts_into_db(contractsList):
    db = get_db()
    cursor = db.cursor()
    
    for contract in contractsList:
        query = """
            INSERT INTO contracts (id, base_salary, contractType, employee, end_date, position, start_date, status)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE
                base_salary = VALUES(base_salary),
                contractType = VALUES(contractType),
                employee = VALUES(employee),
                end_date = VALUES(end_date),
                position = VALUES(position),
                start_date = VALUES(start_date),
                status = VALUES(status)
        """
        contract_id = contract['id']
        base_salary = contract['base_salary']
        contractType = json.dumps(contract['contractType'])  # Convertir el diccionario a JSON
        employee = json.dumps(contract['employee'])  # Convertir el diccionario a JSON
        end_date = contract['end_date']
        position = json.dumps(contract['position'])  # Convertir el diccionario a JSON
        start_date = contract['start_date']
        status = contract['status']

        cursor.execute(query, (contract_id, base_salary, contractType, employee, end_date, position, start_date, status))
    
    db.commit()
    cursor.close()

@bp.route('/contracts/db', methods=['GET'])
def getAllContractsFromDB():
    db = get_db()
    cursor = db.cursor(dictionary=True)

    query = "SELECT * FROM contracts"
    cursor.execute(query)
    contracts = cursor.fetchall()

    for contract in contracts:
        contract['contractType'] = json.loads(contract['contractType'])
        contract['employee'] = json.loads(contract['employee'])
        contract['position'] = json.loads(contract['position'])

    cursor.close()

    return jsonify(contracts) 

@bp.route('/contracts/generate', methods=['POST'])
def generateContracts():
    fake = Faker()
    contractsList = []

    for _ in range(1000):
        contract_id = str(uuid.uuid4())
        start_date = fake.date_between(start_date='-1y', end_date='today')
        end_date = start_date + timedelta(days=365)
        base_salary = round(fake.random_number(digits=4), 2)
        status = fake.boolean()

        employee = {
            "id": str(uuid.uuid4()),
            "names": fake.first_name(),
            "last_names": fake.last_name(),
            "genre": fake.random_element(elements=('M', 'F')),
            "ci": fake.random_number(digits=8),
            "cellphone": fake.random_number(digits=9),
            "address": fake.address(),
            "birthdate": fake.date_of_birth(minimum_age=18, maximum_age=65).strftime('%Y-%m-%d'),
            "code": "EMP" + str(fake.random_number(digits=3)),
            "profession": {
                "id": str(uuid.uuid4()),
                "profession": fake.job()
            }
        }

        position = {
            "id": str(uuid.uuid4()),
            "position": fake.job(),
            "departmentBranch": {
                "id": str(uuid.uuid4()),
                "department": {
                    "id": str(uuid.uuid4()),
                    "department": fake.random_element(elements=('Contabilidad', 'IT', 'Ventas', 'Recursos Humanos'))
                },
                "branch": {
                    "id": str(uuid.uuid4()),
                    "branch": fake.random_element(elements=('Manzana 40 - Empresa', 'Sucursal Centro', 'Sucursal Norte'))
                }
            }
        }

        contract_type = {
            "id": str(uuid.uuid4()),
            "type": fake.random_element(elements=('Tiempo Completo', 'Medio Tiempo', 'Temporal'))
        }

        contract = {
            "id": contract_id,
            "base_salary": base_salary,
            "contractType": contract_type,
            "employee": employee,
            "end_date": end_date.strftime('%Y-%m-%d'),
            "position": position,
            "start_date": start_date.strftime('%Y-%m-%d'),
            "status": status
        }

        contractsList.append(contract)

    insert_contracts_into_db(contractsList)

    return jsonify({'message': '1000 contratos generados e insertados exitosamente.'}), 201