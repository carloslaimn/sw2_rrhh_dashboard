from flask import Flask
import os

def create_app():
    app = Flask(__name__)
    #app.config['SECRET_KEY'] = 'sw2RRHH'
    #app.config['MYSQL_HOST'] = 'localhost'
    #app.config['MYSQL_USER'] = 'root'
    #app.config['MYSQL_PASSWORD'] = 'root'
    #app.config['MYSQL_DB'] = 'dashboard_rrhh'
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY', 'sw2RRHH')
    app.config['MYSQL_HOST'] = os.getenv('MYSQL_HOST', 'db')  # Cambiar 'localhost' a 'db'
    app.config['MYSQL_USER'] = os.getenv('MYSQL_USER', 'root')
    app.config['MYSQL_PASSWORD'] = os.getenv('MYSQL_PASSWORD', 'root')
    app.config['MYSQL_DB'] = os.getenv('MYSQL_DB', 'dashboard_rrhh')
    with app.app_context():
        from .controllers import hello_controller, contracts_controller, dashboard_controller
        app.register_blueprint(hello_controller.bp)
        app.register_blueprint(contracts_controller.bp)
        app.register_blueprint(dashboard_controller.dashboard_bp)

    from .db import get_db, close_db
    app.teardown_appcontext(close_db)

    return app
